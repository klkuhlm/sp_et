import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from datetime import tzinfo
import dateutil

load_parsed = False

# http://matplotlib.1069221.n5.nabble.com/datestr2num-dateutil-parse-and-timezone-problems-td11305.html
def dsn(d,tzinfos={'PDT':-7*60*60}): 
    """ 
    Convert a date string to a datenum using dateutil.parser.parse 
    d can be a single string
    """ 
    dt = dateutil.parser.parse(d,tzinfos=tzinfos) 
    return mpd.date2num(dt)

# read in water level data from 2017
# begins at 15-sec, but is mostly 15-min data
# 1=date/time, 3=temp (C), 4=pressure (psi)
if not load_parsed:
   wl = []
   fh = open('Chorro_pz1b_2017_data.csv','r')
   lines = fh.readlines()[28:]
   fh.close()
   for line in lines:
      f = line.split(',')
      date = f[1] + ' PDT'
      wl.append([dsn(date),float(f[4])])
   wl = np.array(wl)
   np.savetxt('wl.dat',wl)
else:
   wl = np.loadtxt('wl.dat')
print(wl.shape)

# read in airport 5-minute weather data
# 1=date/time, 2=altimiter (inHg)
if not load_parsed:
   ksbp = []
   fh = open('KSBP_2017.csv','r')
   lines = fh.readlines()[8:]
   fh.close()
   for line in lines:
       f = line.split(',')
       if len(f[2]) > 1:  # there are some summary rows in table without data
           ksbp.append([dsn(f[1]),float(f[2])])
   ksbp = np.array(ksbp)
   np.savetxt('ksbp.dat',ksbp)
else:
   ksbp = np.loadtxt('ksbp.dat')
print(ksbp.shape)

## 3: date, 4: time, 12: vapor pressure (mbar)
## read in SLO campus hourly weather data
#if not load_parsed:
#   cimis = []
#   fh = open('cimis_water_hourly_2017.csv','r')
#   lines = fh.readlines()[1:]
#   fh.close()
#   for line in lines:
#      f = line.split(',')
#      if f[4][0:2] == '24':
#         hr = '23' # they call midnight 24 rather than 00
#         min = '59:59.99'
#      else:
#         hr = f[4][0:2]
#         min = '00'
#      datetime = dsn('%s %s:%s PDT' % (f[3],hr,min))
#      vp = float(f[12])
#
#      cimis.append([datetime,vp])
#   cimis = np.array(cimis)
#   np.savetxt('cimis.dat',cimis)
#else:
#   cimis = np.loadtxt('cimis.dat')
#print(cimis.shape)


# read TSprog tide data (15-min synthetic data)
if not load_parsed:
   et = []
   fh = open('SLO_2017.TSF','r')
   lines = fh.readlines()[24:]
   fh.close()
   for line in lines:
      f = line.split()
      ds = dsn('%s/%s/%s %s:%s:%s PDT' % tuple(f[0:6]))
      v = float(f[7])
      et.append([ds,v])

   et = np.array(et)
   np.savetxt('et.dat',et)
else:
   et = np.loadtxt('et.dat')
print(et.shape)
   
fig = plt.figure(1,figsize=(14,5))
ax1 = fig.add_subplot(311)
ax2 = fig.add_subplot(312)
#ax3 = fig.add_subplot(413)
ax4 = fig.add_subplot(313)
ax1.plot_date(wl[:,0],wl[:,1],'k-')
ax2.plot_date(ksbp[:,0],ksbp[:,1],'r-')
#ax3.plot_date(cimis[:,0],cimis[:,1],'b-')
ax4.plot_date(et[:,0],et[:,1],'g-')

date_range = [wl[0,0],wl[-1,0]]
ax1.set_xlim(date_range)
ax2.set_xlim(date_range)
#ax3.set_xlim(date_range)
ax4.set_xlim(date_range)

ax1.set_title('~15-min data')
ax1.set_ylabel('water level (psi)')
ax1.set_xticks([])
ax2.set_title('5-min data')
ax2.set_ylabel('altimieter (in Hg)')
ax2.set_xticks([])
#ax3.set_title('1-hr data')
#ax3.set_ylabel('vapor pressure (mbar)')
#ax3.set_xticks([])
ax4.set_title('15-min synthetic data')
ax4.set_ylabel('earth tide prediction')

plt.tight_layout()
plt.savefig('data_2017.png')
plt.close(1)

# 
fig = plt.figure(2,figsize=(14,5))
ax1 = fig.add_subplot(311)
ax2 = fig.add_subplot(312)
#ax3 = fig.add_subplot(413)
ax4 = fig.add_subplot(313)
DAY2MIN = 24.0*60.0 
ax1.plot_date(wl[1:,0],(wl[1:,0]-wl[:-1,0])*DAY2MIN,'k-')
ax2.plot_date(ksbp[1:,0],(ksbp[1:,0]-ksbp[:-1,0])*DAY2MIN,'r-')
#ax3.plot_date(cimis[1:,0],(cimis[1:,0]-cimis[:-1,0])*DAY2MIN,'b-')
ax4.plot_date(et[1:,0],(et[1:,0]-et[:-1,0])*DAY2MIN,'g-')

ax1.set_yscale('log')
ax2.set_yscale('log')
#ax3.set_yscale('log')
ax4.set_yscale('log')

date_range = [wl[0,0],wl[-1,0]]
ax1.set_xlim(date_range)
ax2.set_xlim(date_range)
#ax3.set_xlim(date_range)
ax4.set_xlim(date_range)

ax1.set_xticks([])
ax2.set_xticks([])
#ax3.set_xticks([])

plt.tight_layout()
plt.savefig('dt_2017.png')
plt.close(2)
   
