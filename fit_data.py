import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpd

nBaroIntvec = [0,2,4,6,8,10,12,14,16]
nETfreqvec = [0,3,6,9,12,13,14,15]

baro_yrange = [-0.2,0.6]

B,E = np.meshgrid(nBaroIntvec,nETfreqvec)
np.savetxt('overall_results_nBaroInt.txt',B,fmt='%i')
np.savetxt('overall_results_nETfreq.txt',E,fmt='%i')

overall_results = np.zeros((len(nBaroIntvec),len(nETfreqvec),2),np.float32)

## https://stackoverflow.com/questions/6518811/interpolate-nan-values-in-a-numpy-array
def nan_helper(y):
    return np.isnan(y), lambda z: z.nonzero()[0]


for jjj,nBaroInt in enumerate(nBaroIntvec):
    for iii,nETfreq in enumerate(nETfreqvec):

        print '#########################################'
        print nBaroInt,nETfreq
        
        # 1) read in pre-parsed data from plotting script
        wl = np.loadtxt('wl.dat')
        ksbp = np.loadtxt('ksbp.dat')
        et = np.loadtxt('et.dat')

        # 2) save data into uniform 15-minute spacing
        # get beginning and end from wl dataset
        # get uniform spacing from synthetic earth tide dataset (not using this as data anymore)
        t0 = wl[0,0]
        t1 = wl[-1,0]

        # 15-minute spaced data
        etm = np.logical_and(et[:,0] >= t0,et[:,0] < t1)
        n = etm.sum()

        # container for evenly-sampled data
        # 1: time, 2: water level, 3: baro, 4: earth tide
        d = np.empty((n,4),dtype=np.float64)
        idx = np.empty((n,3),dtype=np.int)
        d[:,0] = et[etm,0]

        # from eq-finder.py
        #nBaroInt = 10 # number of 15-minute intervals of baro data (4=1 hour) + t=0 to go back into past at any time
        #nETfreq = 5 # number of Earth-tide frequencies to consider (5 works best)
        nETInt = 1 # number of shifted ET signals to use (only 1 makes sense)

        # http://www.ngs.noaa.gov/GRD/GPS/Projects/OLT/Agu.99spring/results.html
        # http://en.wikipedia.org/wiki/Earth_tide
        # periods of 5 major components (hours)

        DiY = 365.242199 # days in a year

        etnames = ['O1','M2','K1','N2','S2',
                   'K2', 'P1', 'Mf','Mm','Ssa',
                   'Lunar','phi1','psi1','S1','Sa']

        earthtideperiods = [25.819, 12.421, 23.934, 12.658, 12.000,
                            11.967, 24.066, 13.661*24.0, 27.555*24.0, DiY*12.0,
                            18.613*DiY*24.0, 23.804, 23.869, 24.000, DiY*24.0,]
        earthtidefreq = [24.0/T for T in earthtideperiods] # convert to frequencies (1/days)

        # write header
        #print ('i,well,formation,residual,constant,ratio before/after var,slope baro response,')
        #print (','.join(['baro (%i min)' % (i*15) for i in range(nBaroInt)]) + ',')
        #print (','.join(['amplitude %s (psi),phase %s (degrees)' %
        #                 (ET,ET) for ET in etnames[:nETfreq]]) + '\n')
            
        dt = 1.0/(24.0*4.0) # 15-min in days
        dt2 = dt/2.0 # half a dt

        for j in range(n):
            t = d[j,0]

            ### mask for all the water level values that fall in 15-minute window
            ##wlm1 = np.logical_and(wl[:,0] >= t-dt2, wl[:,0] < t+dt2)
            ##if (t - wl[wlm1,0]) < epsilon:
            ##    # are these close?
            ##    d[j,1] = np.mean(wl[wlm1,1])
            ##
            ##else:
            # mask for all the water level values that fall in 30-minute window
            # centered on the current time
            wlm = np.logical_and(wl[:,0] >= t-dt, wl[:,0] < t+dt)
            d[j,1] = np.interp(t,wl[wlm,0],wl[wlm,1]) # interpolate, to prevent time shifting?

            #wlm = np.logical_and(wl[:,0] >= t-dt2, wl[:,0] < t+dt2)
            #d[j,1] = np.mean(wl[wlm,1])
                
            idx[j,0] = 1 # number of values being averaged

            # same for barometric pressure (often is 5-min -> 15-min)
            ksbpm = np.logical_and(ksbp[:,0] >= t-dt2, ksbp[:,0] < t+dt2)

            d[j,2] = np.mean(ksbp[ksbpm,1]) 
            idx[j,1] = ksbpm.sum() # number of values being averaged
            
            # TSoft earth tide data are synthetic (not using directly)
            d[j,3] = et[etm,1][j]
            idx[j,2] = 1


        #np.savetxt('nointerp-evenly-spaced.txt',d,fmt='%.14g') # this has nan values where no data

        # go back through and handle intervals (baro) where there were
        # no datapoints in a 15-min window

        y = d[:,2]

        nans,x = nan_helper(y)
        y[nans] = np.interp(x(nans), x(~nans), y[~nans])

        d[:,2] = y[:]

        ##np.savetxt('evenly-spaced.txt',d,fmt='%.14g') # this is updated/interpolated
        ##np.savetxt('idx.txt',idx,fmt='%i')
        date_range = [wl[0,0],wl[-1,0]]

        if 0:
            # compare initial datasets to evenly spaced ones
            fig = plt.figure(1,figsize=(14,5))
            ax1 = fig.add_subplot(311)
            ax2 = fig.add_subplot(312)
            ax4 = fig.add_subplot(313)
            ax1.plot_date(wl[:,0],wl[:,1],'k-')
            ax1.plot_date(d[:,0],d[:,1],'r:')
            ax2.plot_date(ksbp[:,0],ksbp[:,1],'r-')
            ax2.plot_date(d[:,0],d[:,2],'g:')
            ax4.plot_date(et[:,0],et[:,1],'g-')
            ax4.plot_date(d[:,0],d[:,3],'k:')

            ax1.set_xlim(date_range)
            ax2.set_xlim(date_range)
            ax4.set_xlim(date_range)

            ax1.set_ylabel('WL (psi)')
            ax1.set_xticks([])
            ax2.set_ylabel('baro (in Hg)')
            ax2.set_xticks([])
            ax4.set_ylabel('ET')

            plt.tight_layout()
            ##plt.show()  #< for zooming in and comparing curves
            plt.savefig('check_evenly_spaced_data.png')
            plt.close(1)

        # 3) setup linear regression problem
        ns = nBaroInt + (2*nETfreq)*nETInt
        A = np.empty((n-nBaroInt, ns),np.float64)

        # first ns columns are earth tide signal and ns shifted versions of ET signal
        # second block is barometric signal

        # 2nd order polynomial detrend
        z = np.polyfit(d[:,0],d[:,1],2)
        p = np.poly1d(z)
        d[:,1] -= p(d[:,0])

        # save detrended water level data
        ##np.savetxt('evenly-spaced.txt',d,fmt='%.16g') # this is updated/interpolated

        ##print 'subtract means:', np.mean(d[:,1]),np.mean(d[:,2])#,np.mean(d[:,3])

        d[:,1] -= np.mean(d[:,1])
        d[:,2] -= np.mean(d[:,2])
        d[:,3] -= np.mean(d[:,3])

        ##print 'scale to unit range:', np.ptp(d[:,1]),np.ptp(d[:,2])#,np.ptp(d[:,3])

        d[:,1] /= np.ptp(d[:,1])
        d[:,2] /= np.ptp(d[:,2])
        d[:,3] /= (np.ptp(d[:,3]))

        # 3) build up A matrix
        # 3.1) barometric signal shifted back in time
        for j in range(nBaroInt):
            # first row in A corresponds to ns-th row in data, since need previous time for shifting
            if j == 0:
                A[:,0] = d[nBaroInt:,2] # cant index to end of vector with "d[-0]"
            else:
                A[:,j] = d[nBaroInt-j:-j,2]

        # 3.2) ET frequencies (sin & cos, which are orthogonal,
        # rather than time-shifted versions which are not)

        for i in range(nETInt):
            if i == 0:
                tvec = 2.0*np.pi*(d[nBaroInt:,0] - t0)
            else:
                tvec = 2.0*np.pi*(d[nBaroInt-i:-i,0] - t0)
            for j in range(nETfreq):
                A[:,nBaroInt + j*2   + i*nETfreq] = np.cos(earthtidefreq[j]*tvec)
                A[:,nBaroInt + j*2+1 + i*nETfreq] = np.sin(earthtidefreq[j]*tvec)

        #print 'DEBUG: show first two rows of A:',A[:2,:]
        np.savetxt('debugA.txt',A)
            
        # 4) solve
        rhs = d[nBaroInt:,1]
        x,res,rank,s = np.linalg.lstsq(A,rhs,rcond=None)
        if iii == 0 and jjj == 0:
            base_residual = res

        overall_results[jjj,iii,:] = np.array([res[0],rank])
        
        print 'x_BARO (%i):' %(nBaroInt,), x[:nBaroInt]
        print 'x_ET 2*(%i):' %(nETfreq,), x[nBaroInt:]
        print 'residual:',res
        print 'rank',rank
        print 'singular values:',s

        # 5) compute residual
        bboth = np.matmul(A,x)

        lw = 0.5
        plt_scale = 5.0

        fig = plt.figure(1,figsize=(14,7))
        ax_data = fig.add_subplot(311)
        ax_results = fig.add_subplot(312)
        ax_residual = fig.add_subplot(313)

        ax_data.plot_date(d[nBaroInt:,0],rhs,'r-',label='observed ($t^2$ detrended)',lw=lw)
        ax_data.plot_date(d[nBaroInt:,0],d[nBaroInt:,2]/plt_scale+0.2,'g-',label='initial baro signal',lw=lw)
        ax_data.plot_date(d[nBaroInt:,0],d[nBaroInt:,3]/plt_scale+0.4,'b:',label='TProg signal',lw=lw)

        # compute apparent earth tide signal from LS results
        etvec = np.zeros_like(rhs)
        for i in range(nETInt):
            if i == 0:
                tvec = 2.0*np.pi*(d[nBaroInt:,0] - t0)
            else:
                tvec = 2.0*np.pi*(d[nBaroInt-i:-i,0] - t0)
                
            for j in range(nETfreq):
                etvec += x[nBaroInt+(2*j)   + i*nETfreq]*np.cos(earthtidefreq[j]*tvec)
                etvec += x[nBaroInt+(2*j)+1 + i*nETfreq]*np.sin(earthtidefreq[j]*tvec)

        # compute apparent total barometric signal from LS results
        barovec = np.zeros_like(rhs)
        for j in range(nBaroInt):
            barovec += x[j]*d[nBaroInt:,2]
                
        ax_results.plot_date(d[nBaroInt:,0],etvec,'b-',label='ET from data',lw=lw)
        ax_results.plot_date(d[nBaroInt:,0],barovec+0.4,'g-',label='baro response from data',lw=lw)

        ax_data.set_title('%i shifted barometric; %i earth-tide frequencies, relative residual=%.2f' %
                          (nBaroInt,nETfreq,res[0]/base_residual))

        ax_residual.plot_date(d[nBaroInt:,0],rhs-bboth,'g-',label='res',lw=lw)

        ax_data.set_xlim(date_range)
        ax_results.set_xlim(date_range)
        ax_residual.set_xlim(date_range)
        ax_results.set_ylabel('wl (scaled)')
        ax_residual.set_ylabel('residual')

        ax_data.legend(loc=0)
        ax_results.legend(loc=0)
        ax_data.set_xticks([])
        ax_results.set_xticks([])

        plt.tight_layout()
        plt.savefig('res_freq_%2.2i_%2.2i.png' % (nBaroInt,nETfreq))
        plt.close(1)

        # plot response functions

        fig = plt.figure(1,figsize=(10,5))
        ax_baro = fig.add_subplot(121)
        ax_et = fig.add_subplot(122, polar=True)

        baroresp = np.cumsum(x[:nBaroInt])
        t_plot  = 15.0*np.arange(nBaroInt)
        ax_baro.plot(t_plot, baroresp)
        ax_baro.axhline(0.0, linewidth=0.25, color='gray')
        ax_baro.set_xlabel('baro lag time (min)')
        ax_baro.set_ylabel('baro response (cumsum)')
        ax_baro.set_ylim(baro_yrange)
        
        for i,f in enumerate(earthtidefreq[:nETfreq]):
            Aj = x[nBaroInt + i*2]
            Bj = x[nBaroInt + i*2 + 1]
            phase = -np.arctan(Bj/Aj)
            amp = np.abs(Aj/(np.cos(phase))) ## << ?

            ax_et.plot(phase,amp,'o',label=etnames[i])

        ax_et.legend(loc=0)
        plt.tight_layout()
        ax_baro.set_title('residual = %.3g' % (res[0]/base_residual))
        plt.savefig('response_%2.2i_%2.2i.png' % (nBaroInt,nETfreq) )
        plt.close(1)

np.savetxt('overall_results_residual.txt',overall_results[:,:,0],fmt='%.6g')
np.savetxt('overall_results_rank.txt',overall_results[:,:,1],fmt='%i')

fig = plt.figure(1)
ax = fig.add_subplot(111)
for j,ETf in enumerate(nETfreqvec):
    ax.plot(nBaroIntvec,overall_results[:,j,0],label='nET=%i' % (ETf))
ax.set_xlabel('nBaroInt')
ax.set_ylabel('relative residual')
ax.legend(loc=0)
plt.savefig('compare.png')
