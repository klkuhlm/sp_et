tic
   %clear all;
   data = load('spdu2_11252019.txt');%Read data file into array named 'data'
   
a1 = 0.226371;   
a2 = -0.0159255; 
a3 = 0.000451308;
a4 = -3.4094;    
   
   T = data(:,1)/(24*3600); %Time vector
   sp4mean = a1*T + a2*(T.^2) + a3*(T.^3) + a4;
   sp4 = data(:,12)-sp4mean;%Data vector
   dt = 30; %Sampling interval (s)
   Fs = 1/dt; %Sampling frequency (Hz)
   Fn = Fs/2; %Nyquist frequency
   
   L = length(sp4); % Length of data vector
   TT = linspace(0,1,L)*dt;
   
   FF = fft(sp4)/L;
   Fv = linspace(0,1,fix(L/2)+1)*Fn;
   
   Iv = 1:length(Fv);          % Index Vector
   figure(1)                   % Plot FFT
   bar((Fv*24*3600), abs(FF(Iv).^2),0.5) %Plot raw power spectrum density
   grid
   xlabel('Frequency (cpd)', 'FontSize',16)
   ylabel('Power (|\Psi|^2)', 'FontSize',16)
   
   txt = 'Diurnal';
   text(0.9,0.0015,txt, 'FontSize',16)

   txt = 'Semidiurnal';
   text(1.8,0.007,txt, 'FontSize',16)
   title('SP Power Spectrum (e-20)', 'FontSize',16)
   set(gca,'FontSize',16);
   axis([0 2.5 0 0.01]);

toc