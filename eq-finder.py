import numpy as np
from scipy.signal import detrend
import matplotlib.dates as md
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from glob import glob
from matplotlib.font_manager import FontProperties

# smaller font for legends
LegFontP = FontProperties()
LegFontP.set_size('small')

formlist = {'C':'CUL', 'M':'MAG', 'B':'BC'}

baroFuture = False
baroSmooth = True
smooth =  0.5  # bigger number increases smoothness

# to make all the polar plots the same r-scale
rmax = 0.04
# make all baro response functions same scale
bmax = 3.0

# **************************************************
# parameters to set
nBaroInt = 13 # number of 15-minute intervals of baro data (4=1 hour) + t=0 to go back into past at any time
nETfreq = 5 # number of Earth-tide frequencies to consider (5 works best)
header = 79 # number of lines of header in Troll csv files (only interested in recent data, so this can be too big)

deltat = 15.0/60.0/24.0  # distance between baro datapoints in days
timeEps = 1.0E-7  # some datapoints are not exactly on hours (due to round-off in calcualtions?)
# **************************************************

# http://www.ngs.noaa.gov/GRD/GPS/Projects/OLT/Agu.99spring/results.html
# http://en.wikipedia.org/wiki/Earth_tide
# periods of 5 major components (hours)

DiY = 365.242199
psi2ft = 2.30897

etnames = ['O1','M2','K1','N2','S2',
           'K2', 'P1', 'Mf','Mm','Ssa',
           'Lunar','phi1','psi1','S1','Sa']

earthtideperiods = [25.819, 12.421, 23.934, 12.658, 12.000,
                    11.967, 24.066, 13.661*24.0, 27.555*24.0, DiY*12.0,
                    18.613*DiY*24.0, 23.804, 23.869, 24.000, DiY*24.0,]
earthtidefreq = [24.0/T for T in earthtideperiods] # convert to frequencies (1/days)


# write header
print ('i,well,formation,residual,constant,ratio before/after var,slope baro response,')
print (','.join(['baro (%i min)' % (i*15) for i in range(nBaroInt)]) + ',')
print (','.join(['amplitude %s (psi),phase %s (degrees)' %
                 (et,et) for et in etnames[:nETfreq]]) + '\n')


begintrain = md.datestr2num('05/11/2017 00:00:00')  # begin barometric training
begindate = md.datestr2num('08/10/2017 00:00:00')  # end of training/ beginning of testing intervals

ETfig = plt.figure(2,figsize=(8,8))
axculET = ETfig.add_subplot(111,polar=True)

fig = plt.figure(1,figsize=(16,8))
##axmag = fig.add_subplot(611)
##axcul1 = fig.add_subplot(612)
##axcul2 = fig.add_subplot(613)
##axcul3 = fig.add_subplot(614)
##axbc = fig.add_subplot(615)
axclose = fig.add_subplot(111) #616)

allbarod,allbarop = np.loadtxt('wl.txt')
meanbaro = allbarop.mean()

tzero = begintrain

def setupAmatrix(begindt,enddt):
    
    for i in range(nBaroInt):
        m = np.logical_and(allbarod[:] >= begindt -(deltat*i)-timeEps,
                           allbarod[:] <= enddt   -(deltat*i)+timeEps)
        if i == 0:
            n = allbarop[m][::4].shape[0]
            
            # second dim of A is 
            # number of barometric levels (maybe +1 into the future) + 
            # 2 x number ET frequencies (sin & cos) +
            # a constant shift term
            
            nd = nBaroInt+1+nETfreq*2
            if baroFuture:
                nd += 1
            if baroSmooth:
                if baroFuture:
                    extra = nBaroInt 
                else:
                    extra = nBaroInt - 1
            else:
                extra = 0

            A = np.ones((n+extra,nd)) 
            
            if baroSmooth:
                # smoothing section of A matrix is mostly zeros
                # with a single unit entry for each variable (zero RHS)
                A[n:,:] = 0.0

            # earth-tide frequencies
            tf = allbarod[m][-1]
            tvec = 2*np.pi*(allbarod[m][::4] - tzero)

            if baroFuture:
                # last column - so it doesn't mess up indexing elsewhere
                # include baro one time-step into the future (maybe clocks aren't synced correctly?)
                mf = np.logical_and(allbarod[:] >= begindt +deltat-timeEps,
                                    allbarod[:] <= enddt   +deltat+timeEps)
                A[:n,-1] = allbarop[mf][::4] - meanbaro
                if baroSmooth:
                    # the future baro value should default to zero
                    A[-1,-1] = smooth

            for j in range(nETfreq):
                A[:n,nBaroInt+j*2+1] = np.cos(earthtidefreq[j]*tvec)
                A[:n,nBaroInt+j*2+2] = np.sin(earthtidefreq[j]*tvec)
        A[:n,i+1] = allbarop[m][::4] - meanbaro
        if baroSmooth and i > 0:
            # only apply smoothing to entries
            # after the first one
            #print i,n+i,i+1,A.shape
            A[n+i-1,i+1] = smooth
    return (A,extra)

Atr,extra = setupAmatrix(begintrain,begindate)
#Ate,extra = setupAmatrix(begindate,end1date)

# dates and pressure values
d,p = np.loadtxt('wl.txt',unpack=True)

    hourly = np.remainder(d[:]*24.0,1.0) == 0.0
    if (np.logical_not(hourly)).sum() > 0:
        # weed out extra datapoints not on hour
        d = d[hourly]
        p = p[hourly]

    # masks for training and testing intervals
    mtr = np.logical_and(d[:] >= begintrain, d[:] <= begindate)
    mte = np.logical_and(d[:] >= begindate, d[:] <= end1date)

    print j,well,formation

    # compute coefficients using training interval
    try:
        rhs = np.concatenate((detrend(p[mtr]),np.zeros((extra,))),axis=0)
        solution,residues,rank,s = np.linalg.lstsq(Atr[:,:],rhs)
    except:
        print 'incorrect amount of training data, skipping!'
        continue
    fhout.write('%i,%s,%s,%.5e,' % (j,well,formlist[formation],residues[0]))

    # compute residuals using testing interval
    n = mte.sum()
    res = detrend(p[mte]) - np.dot(Ate[:n,:],solution)

    # plot training data intervals
    figEach = plt.figure(9,figsize=(18,6))
    gs = gridspec.GridSpec(1,8)
    gs.update(wspace=2.0)
    axEach = figEach.add_subplot(gs[0:4]) # main plot is 3 wide, baro response plot is 1 wide, rose is 1 wide
    axEach.plot_date(d[mtr],p[mtr],'r-',label='raw')
    axEach2 = axEach.twinx()
    axEach2.plot_date(d[mtr],np.dot(Atr[:mtr.sum(),:],solution)-detrend(p[mtr]),'b-',label='cleaned',alpha=0.5)
    axEach.set_ylabel('raw Troll (psi)',color='red')
    axEach2.set_ylabel('cleaned Troll (psi)',color='blue')
    ymin,ymax = axEach2.get_ylim()
    if abs(ymax-ymin) < 0.25:
        axEach2.set_ylim([-0.125,0.125])
    axEach.set_title('%s %s' % (well,formation))
    axEach.xaxis.set_major_locator(md.DayLocator(interval=7))
    axEach.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
    axEach.xaxis.set_minor_locator(md.DayLocator())
    
    axbaro = figEach.add_subplot(gs[4:6])
    if baroFuture:
        baroresp = np.cumsum(np.concatenate((np.atleast_1d(solution[-1]),solution[1:nBaroInt+1]),axis=0))
        x = 15*(np.arange(nBaroInt+1)-1)
        axbaro.plot(x,baroresp)
    else:
        baroresp = np.cumsum(solution[1:nBaroInt+1])
        x = 15*(np.arange(nBaroInt))
        axbaro.plot(x,baroresp)
    axbaro.axhline(0.0,linewidth=0.25,color='gray')
    ymin,ymax = axbaro.get_ylim()
    axbaro.set_ylim([ymin,bmax])
    axbaro.set_xlabel('baro time lag (minutes)')
    axbaro.set_ylabel('baro response (cumsum)')

    axET = figEach.add_subplot(gs[6:8],polar=True)

    aj = []
    zetaj = []
    for i,f in enumerate(earthtidefreq[:nETfreq]):
        Aj = solution[nBaroInt+1+(i*2)]
        Bj = solution[nBaroInt+1+(i*2)+1]
        zetaj.append(-np.arctan(Bj/Aj))  # in radians
        aj.append(Aj/(np.cos(zetaj[-1])))
        amp = aj[-1]
        phase = zetaj[-1]
        axET.plot(phase,amp,'o',color=etcolors[i],label=etnames[i])
        etaxes[formation].plot(phase,amp,'o',color=etcolors[i],label=etnames[i],markersize=5)
        
    zetaj = np.array(zetaj)

    # compute ratio of variance of linearly detrended original
    # to variance of residual over the training interval
    origp = np.var(detrend(p[mtr]))
    newp = np.var(np.dot(Atr[:-extra,:],solution)-detrend(p[mtr]))
    slope = np.polyfit(x[:-1],baroresp[:-1],1)[0] # get linear slope of barometric response function (drop last value)

    # print baro/earth-tide fitting results to file (for use by other script)
    # include the reponse 15 min into future as first one
    fhout.write('%.7e,%.7e,%.7e,' % (solution[0],origp/newp,slope) +
                ','.join(['%.6e' % w for w in baroresp]) + ',')
    
    fhout.write(','.join(['%14.7e,%9.4f' % w for w in zip(aj,zetaj*180.0/np.pi)])+'\n')  # convert to degrees for table

    #axET.set_rscale('log')
    box = axET.get_position()
    axET.set_position([box.x0, box.y0 + box.height*0.1,
                       box.width, box.height*0.9])
    axET.legend(loc='upper center',bbox_to_anchor=(0.5,-0.05),
                ncol=5,numpoints=1,prop=LegFontP)
    axET.set_ylim([0,rmax])
    axET.set_rgrids([rmax*0.25,rmax*0.5,rmax*0.75,rmax],fmt='%.2g')
    figEach.savefig('training-data-%s-%s.png' % (well,formation))
    plt.close(9)

    # plot up de-trended data
    meanres = (res)[:24].mean()

      if formation == 'C':
          culn += 1
          if culn < ncolors+1:
              axcul1.plot_date(d[mte],psi2ft*(res-meanres),'-',linewidth=0.75,
                               color=colors[culn-1],label=well)
          elif culn < 2*ncolors+1:
              axcul2.plot_date(d[mte],psi2ft*(res-meanres),'-',linewidth=0.75,
                               color=colors[culn%ncolors-1],label=well)
          else:
              axcul3.plot_date(d[mte],psi2ft*(res-meanres),'-',linewidth=0.75,
                               color=colors[culn%ncolors-1],label=well)

      elif formation == 'M':
          magn += 1
          axmag.plot_date(d[mte],psi2ft*(res-meanres),'-',linewidth=0.75,
                          color=colors[magn%ncolors-1],label=well)
      elif formation == 'B':
          bcn += 1
          axbc.plot_date(d[mte],psi2ft*(res-meanres),'-',linewidth=0.75,
                         color=colors[bcn-1],label=well)
    if well in closewells:
        closen += 1
        axclose.plot_date(d[mte],psi2ft*(res-meanres),'-',linewidth=0.75,
                          color=colors[closen],label=well)

fhout.close()


# legend for type of density measurement
# while removing duplicate entries from legend
handles,labels = etaxes['B'].get_legend_handles_labels()
newhandles = []
newlabels = []
for h,l in zip(handles,labels):
    if not l in newlabels:
        newhandles.append(h)
        newlabels.append(l)

box = etaxes['B'].get_position()
etaxes['B'].set_position([box.x0, box.y0 + box.height*0.1,
                          box.width, box.height*0.9])
etaxes['B'].legend(loc='upper center',bbox_to_anchor=(0.5,-0.05),
                   ncol=5,numpoints=1,prop=LegFontP)


for f in formlist.keys():
    etaxes[f].set_ylim([0,rmax])
    etaxes[f].set_rgrids([rmax*0.25,rmax*0.5,rmax*0.75,rmax],fmt='%.2g')
    etaxes[f].set_title('%s earth-tide components' % formlist[f])

leg = etaxes['B'].legend(newhandles,newlabels,loc=0,prop=LegFontP,numpoints=1,scatterpoints=1,ncol=5)
ETfig.savefig('earth-tide-components.png')
plt.close(2)

yrange = [-0.5,0.5]

##axmag.set_ylabel('relative Magenta (ft $H_2O$)')
##axmag.legend(loc=0,prop=LegFontP,ncol=6)
##axmag.xaxis.set_major_locator(md.DayLocator())
##axmag.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
##axmag.xaxis.set_minor_locator(md.HourLocator(interval=6))
##axmag.set_ylim(yrange)
##axmag.axvline(x=eqdate)
##
##axcul1.set_ylabel('relative Culebra(1) (ft $H_2O$)')
##axcul1.set_ylim(yrange)
##axcul1.legend(loc='upper left',prop=LegFontP,ncol=6)
##axcul1.xaxis.set_major_locator(md.DayLocator())
##axcul1.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
##axcul1.xaxis.set_minor_locator(md.HourLocator(interval=6))
##axcul1.axvline(x=eqdate)
##
##axcul2.set_ylabel('relative Culebra(2) (ft $H_2O$)')
##axcul2.set_ylim(yrange)
##axcul2.legend(loc='upper right',prop=LegFontP,ncol=6)
##axcul2.xaxis.set_major_locator(md.DayLocator())
##axcul2.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
##axcul2.xaxis.set_minor_locator(md.HourLocator(interval=6))
##axcul2.axvline(x=eqdate)
##
##axcul3.set_ylabel('relative Culebra(3) (ft $H_2O$)')
##axcul3.set_ylim(yrange)
##axcul3.legend(loc='upper right',prop=LegFontP,ncol=6)
##axcul3.xaxis.set_major_locator(md.DayLocator())
##axcul3.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
##axcul3.xaxis.set_minor_locator(md.HourLocator(interval=6))
##axcul3.axvline(x=eqdate)
##
##axbc.set_ylabel('relative Bell Canyon (ft $H_2O$)')
##axbc.set_ylim(yrange)
##axbc.legend(loc=0,prop=LegFontP)
##axbc.xaxis.set_major_locator(md.DayLocator())
##axbc.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
##axbc.xaxis.set_minor_locator(md.HourLocator(interval=6))
##axbc.axvline(x=eqdate)

axclose.set_ylabel('SNL-16 and IMC-461 (ft $H_2O$)')
axclose.legend(loc='lower right',prop=LegFontP,ncol=4)
axclose.xaxis.set_major_locator(md.DayLocator())
axclose.xaxis.set_major_formatter(md.DateFormatter('%m/%d'))
axclose.xaxis.set_minor_locator(md.HourLocator(interval=6))
axclose.axvline(x=eqdate,color='black')

axclose.set_xlabel('date')
plt.suptitle('DRAFT Troll pressures in WIPP wells around 3/18/2012 DRAFT')
fig.savefig('troll_pressures_march.eps')

