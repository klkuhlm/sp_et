tic
   %clear all;
   data = load('chorro-long-data2.txt');%Read data file into array named 'data'
   
   a1 = -6.29607e-005;
   a2 = -1.81472e-005;
   a3 = 1.05522e-007;
   a4 = 1.28569;
   t0 = 40;
   
   T = data(:,1)/(24*60); %Time vector
   pmean = a1*T + a2*(T.^2) + a3*(T.^3) + a4;
   p1res = data(:,3) - pmean%Data vector
   dt = 15*60; %Sampling interval (s)
   Fs = 1/dt; %Sampling frequency (Hz)
   Fn = Fs/2; %Nyquist frequency
   L = length(p1res); % Length of data vector
   
   LF = size(p1res,1);
   TT = linspace(0,1,LF)*dt;
   
   %figure(1) %Plot data
   %plot(T,p1res);
   %grid
   
   FF = fft(p1res)/LF;
   Fv = linspace(0,1,fix(LF/2)+1)*Fn;
   
   Iv = 1:length(Fv);          % Index Vector
   figure(2)                   % Plot FFT
   bar(Fv*24*3600, abs(FF(Iv)).^2, 0.5)
   grid
   xlabel('Frequency (cpd)', 'FontSize',14)
   ylabel('Power (|H_R(t)|^2)', 'FontSize',14)
   
   txt = 'Diurnal';
   text(0.9,2.6e-6,txt, 'FontSize',14)
   txt = 'Semidiurnal';
   text(1.8,1.5e-6,txt, 'FontSize',14)
   title('Power Spectrum of Head Residuals', 'FontSize',14)
   set(gca,'FontSize',14);
   axis([0 2.5 0 3e-6]);

toc